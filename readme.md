### Photo Gallery

---
This is a photo gallery built with Laravel. It is possible to create albums with a cover image and if click on the albums it will take us to a listing of photos of the album. It is possible to create albums, to upload and delete images.

---
Used technology:

* PHP
* Laravel
* Foundation Framework